// CS3241Lab1.cpp: Defines the entry point for the console application.

// For the one who adores the new Coconut Pokémon from Alola.
// Sun Wang Jun (A0110546R)

#include <algorithm>  // min, max.
#include <cmath>
#include <iostream>

/* Include header files depending on platform. */
#ifdef _WIN32
  #include "glut.h"
  #define M_PI 3.14159
#elif __APPLE__
  #include <OpenGL/gl.h>
  #include <GLUT/GLUT.h>
#endif

using namespace std;


float M_DEG_TO_RAD = M_PI / 180;

float alpha, k, tx, ty, mouseX, mouseY;
void resetSceneValues() {
  alpha = 0.0, k = 1.8;
  tx = 0.0, ty = -16;  // -16 to show only Exeggutor head.
  mouseX = 0.0, mouseY = 0.0;
}

void renderCurveFill(const unsigned int divisions, const unsigned int peak,
                     const float length, const float bend,
                     const float thickness) {
  // Curve will be rendered horizontally - the user will rotate it himself.
  float x = 0;
  float y = 0;
  float yThickness = 0;
  float xVertexWidth = length / divisions;

  // Draw towards peak.
  int sumToPeak = 0.5 * peak * (peak + 1);  // Arithmetic sum from 1 to peak.
  float bendDelta = bend / sumToPeak;
  float thicknessDelta = thickness / sumToPeak;
  for (int i = 0; i < peak; i++) {
    x = i * xVertexWidth;
    glVertex2f(x, y);
    glVertex2f(x, y + yThickness);
    y += bendDelta * (peak - i);
    yThickness += thicknessDelta * (peak - i);
  }

  // Add vertices for peak point.
  glVertex2f(peak * xVertexWidth, y);
  glVertex2f(peak * xVertexWidth, y + yThickness);

  // Draw from peak.
  int sumFromPeak = 0.5 * (divisions - peak) * (divisions - peak + 1);
  bendDelta = bend / sumFromPeak;
  thicknessDelta = thickness / sumFromPeak;
  for (int i = divisions - peak; i >= 0; i--) {
    x = (divisions - i) * xVertexWidth;
    y -= bendDelta * (divisions - peak - i);
    yThickness -= thicknessDelta * (divisions - peak - i);
    glVertex2f(x, y);
    glVertex2f(x, y + yThickness);
  }
}

void drawMimikyuBase() {
  glPushMatrix();
  glTranslated(0, 0, 0.1);

  glBegin(GL_TRIANGLE_FAN); {
    glColor3ub(0, 0, 0);
    glVertex2f(0.25, 1);  // Tip of the base.
    glVertex2f(-3, -2.5);
    glVertex2f(-1, -2);
    glVertex2f(-1.75, -3);
    glVertex2f(-0.5, -2.5);
    glVertex2f(0, -3.25);
    glVertex2f(0.75, -2.5);
    glVertex2f(1.75, -2.75);
    glVertex2f(2, -2.25);
    glVertex2f(3.25, -2.25);
  } glEnd();

  glPopMatrix();
}

void drawExeggutorCostumeBase() {
  glPushMatrix();
  glTranslated(0, 0, 0.2);

  glBegin(GL_TRIANGLE_FAN); {
    glColor3ub(0xbf, 0x8c, 0x65);

    // Arc for top costume.
    float radius = 2.65;
    float baseY = -0.3;
    for (float deg = -19; deg < 200; deg++) {
      float rad = deg * M_DEG_TO_RAD;
      float x = radius * cos(rad);
      float y = baseY + radius * sin(rad) * 1.2;
      glVertex2f(x, y);
    }

    // Costume leggings.
    glVertex2f(-3.25, -2.25);
    glVertex2f(-2.75, -1.75);
    glVertex2f(-2.25, -2.75);
    glVertex2f(-1.75, -1.55);
    glVertex2f(-1.25, -3);
    glVertex2f(-1, -2.25);
    glVertex2f(0.5, -3);
    glVertex2f(1, -2.25);
    glVertex2f(2.25, -2.75);
    glVertex2f(2.5, -2);
    glVertex2f(3.25, -2);
  } glEnd();

  glPopMatrix();
}

void renderMimikyuEye() {
  // TODO: This can be a generic circle function.
  glPushMatrix();

  glBegin(GL_TRIANGLE_FAN); {
    glColor3ub(0, 0, 0);

    float radius = 0.17;
    for (float deg = 0; deg < 360; deg++) {
      float rad = deg * M_DEG_TO_RAD;
      float x = radius * cos(rad);
      float y = radius * sin(rad) * 2.4;
      glVertex2f(x, y);
    }
  } glEnd();

  glPopMatrix();

}

void drawMimikyuEyes(float sway) {
  glPushMatrix();

  float distanceFromMiddle = 0.8;
  float yDisplacement = 0.5;
  glTranslatef(sway * 8, 0, 0.3);

  glPushMatrix(); {  // Right eye.
    glTranslatef(-1 * distanceFromMiddle, yDisplacement, 0);
    renderMimikyuEye();
  } glPopMatrix();
  glPushMatrix(); {  // Left eye.
    glTranslatef(distanceFromMiddle, yDisplacement, 0);
    renderMimikyuEye();
  } glPopMatrix();

  glPopMatrix();
}

void renderYellowHead(float radius, float elongation) {
  glPushMatrix();

  glBegin(GL_TRIANGLE_FAN); {
    for (float deg = 0; deg < 360; deg++) {
      float rad = deg * M_DEG_TO_RAD;
      float x = radius * cos(rad);
      float y = radius * sin(rad) * elongation;
      glVertex2f(x, y);
    }
  } glEnd();

  glPopMatrix();
}

void renderCircleScribble(float radius, const int cycles, const float skip,
                          const unsigned int seed) {
  glPushMatrix();
  srand(seed);

  glBegin(GL_LINE_STRIP); {
    float r = radius;
    float rDelta = 0;
    float deg = skip;

    for (int i = 0; i < cycles; i++) {
      float newR = rand() % 1000 / 1000.0 * radius;
      rDelta = (newR - r) / (360.0 / skip);
      float xScale = rand() % 100 / 100.0 * radius + 1 - radius / 4;
      float yScale = rand() % 100 / 100.0 * radius + 1 - radius / 4;
      float xTranslate = rand() % 100 / 100.0 * radius - radius / 4;
      float yTranslate = rand() % 100 / 100.0 * radius - radius / 4;

      for (; deg < 360; deg += skip) {
        float rad = deg * M_DEG_TO_RAD;
        float x = r * cos(rad) * xScale + xTranslate;
        float y = r * sin(rad) * yScale + yTranslate;
        glVertex2f(x, y);
        r += rDelta;
      }

      deg = (int)deg % 360;
    }
  } glEnd();

  glPopMatrix();
}

void drawOpenEye(float radius, float elongation) {
  glPushMatrix();

  glLineWidth(1.2);
  glTranslatef(0.1, 0, 0.4);

  glBegin(GL_LINE_STRIP); {
    glColor3ub(0x66, 0x66, 0x66);
    for (float deg = -36; deg <= 216; deg += 18) {
      float rad = deg * M_DEG_TO_RAD;
      float x = radius * cos(rad);
      float y = radius * sin(rad) * elongation;
      glVertex2f(x, y);
    }
  } glEnd();

  glPointSize(3);
  glBegin(GL_POINTS); {
    glVertex2f(0, 0);
  } glEnd();

  glPopMatrix();
}

void drawRightHeadEyes() {
  glPushMatrix();

  glTranslatef(-0.75, 0.1, 0.3);
  glScalef(0.5, 0.5, 1);
  glRotatef(15, 0, 0, 1);

  glPushMatrix(); {
    glColor3ub(0xff, 0xff, 0xff);
    glBegin(GL_LINE_STRIP); {
      renderCurveFill(10, 10, 1.2, 0, 0.5);
    } glEnd();
  } glPopMatrix();

  glPushMatrix(); {
    glColor3ub(0x66, 0x66, 0x66);
    glTranslatef(0, 0, 0.1);
    glBegin(GL_LINE_STRIP); {
      renderCurveFill(10, 10, 1.2, 0.5, 0);
    } glEnd();
  } glPopMatrix();

  glPointSize(2);
  glTranslatef(0.5, 0, 0.3);
  glBegin(GL_POINTS); {
    glVertex2f(0, 0);
  } glEnd();


  glPopMatrix();
}

void drawLeftHeadEyes() {
  glPushMatrix();

  glTranslatef(0.1, 0.3, 0.3);
  glScalef(0.5, 0.5, 1);
  glRotatef(15, 0, 0, 1);

  glPushMatrix(); {
    glColor3ub(0xff, 0xff, 0xff);
    glBegin(GL_LINE_STRIP); {
      renderCurveFill(10, 5, 1.2, 0, -0.4);
    } glEnd();
  } glPopMatrix();

  glPushMatrix(); {
    glColor3ub(0x66, 0x66, 0x66);
    glTranslatef(0, 0, 0.3);
    glBegin(GL_LINE_STRIP); {
      renderCurveFill(10, 5, 1.2, 0, 0);
    } glEnd();
  } glPopMatrix();

  glPushMatrix(); {
    glPointSize(2);
    glTranslatef(0.75, -0.15, 0.3);
    glBegin(GL_POINTS); {
      glVertex2f(0, 0);
    } glEnd();
  } glPopMatrix();

  glPopMatrix();
}

void drawMiddleHeadEyes() {
  glPushMatrix(); {
    glColor3ub(0xff, 0xff, 0xff);
    glLineWidth(1.6);
    glTranslatef(-0.45, 0.3, 0.3);
    glScalef(0.5, 0.5, 1);
    renderCircleScribble(0.4, 21, 11, 8);
    drawOpenEye(0.45, 1.2);
  } glPopMatrix();

  glPushMatrix(); {
    glColor3ub(0xff, 0xff, 0xff);
    glLineWidth(1.6);
    glTranslatef(0.35, 0.3, 0.3);
    glScalef(0.5, 0.5, 1);
    renderCircleScribble(0.5, 21, 11, 3);
    drawOpenEye(0.55, 1.2);
  } glPopMatrix();
}

void drawExeggutorHeads(float x, float y) {
  glPushMatrix();
  glTranslated(0, 0, 0.1);
  glEnable(GL_LINE_SMOOTH);

  glPushMatrix(); {  // Right head.
    glTranslatef(x - 1.2, y - 0.1, 0);
    glColor3ub(0xf6, 0xf9, 0x9d);
    renderYellowHead(1, 0.95);
    drawRightHeadEyes();

    glPushMatrix(); {
      glColor3ub(0xca, 0x6d, 0x76);
      glTranslatef(-0.86, -0.5, 0.3);
      glRotatef(25, 0, 0, 1);
      glBegin(GL_TRIANGLE_STRIP); {
        renderCurveFill(20, 7, 0.8, 0.05, -0.6);
      } glEnd();
    } glPopMatrix();
  } glPopMatrix();

  glPushMatrix(); {  // Left head.
    glTranslatef(x + 1.2, y - 0.1, 0);
    glColor3ub(0xf6, 0xf9, 0x9d);
    renderYellowHead(1, 1);
    drawLeftHeadEyes();

    glPushMatrix(); {
      glColor3ub(0xca, 0x6d, 0x76);
      glTranslatef(0.33, -0.2, 0.3);
      glRotatef(15, 0, 0, 1);
      glBegin(GL_TRIANGLE_STRIP); {
        renderCurveFill(20, 17, 0.7, 0, -0.2);
      } glEnd();
    } glPopMatrix();
  } glPopMatrix();

  glPushMatrix(); { // Middle head.
    glTranslatef(x, y, 0.1);
    glColor3ub(0xfb, 0xff, 0xb3);
    renderYellowHead(1.1, 1.1);
    drawMiddleHeadEyes();

    glPushMatrix(); {
      glColor3ub(0xff, 0, 0);
      glTranslatef(-0.5, -0.2, 0.3);
      renderCurveFill(10, 5, 1, 0, -0.7);
    } glPopMatrix();

    glPushMatrix(); {
      glColor3ub(0xec, 0x80, 0x8b);
      glTranslatef(-0.6, -0.3, 0.3);
      glBegin(GL_TRIANGLE_STRIP); {
        renderCurveFill(20, 10, 1.2, 0.04, -0.6);
      } glEnd();
    } glPopMatrix();
  } glPopMatrix();

  glPopMatrix();
}

void drawLeaf(const unsigned int divisions, const unsigned int peak,
              const float length, const float bend,
              const float thickness) {
  glPushMatrix(); {
    glBegin(GL_TRIANGLE_STRIP);
    renderCurveFill(divisions, peak, length, bend, thickness);
    glEnd();
  } glPopMatrix();
}

void drawLeaves(float x, float y) {
  glPushMatrix();
  glColor3ub(0x89, 0xe0, 0x75);
  glTranslatef(x, y + 0.3, -0.1);

  glPushMatrix(); {
    glRotatef(-65, 0, 0, 1);
    drawLeaf(20, 10, 3.6, 0.3, 0.35);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(-0.1, -0.3, 0.1);
    glRotatef(-10, 0, 0, 1);
    drawLeaf(20, 12, 4, 0.8, 0.5);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(-0.2, -0.5, 0.1);
    glRotatef(20, 0, 0, 1);
    drawLeaf(20, 8, 5.4, 0.8, 0.55);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(0.2, 0, -0.1);
    glRotatef(85, 0, 0, 1);
    drawLeaf(20, 10, 2.8, 0.2, 0.4);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(0.7, -1.1, 0.1);
    glRotatef(155, 0, 0, 1);
    drawLeaf(20, 15, 4.8, -1, -0.55);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(-0.2, -0.2, 0);
    glRotatef(190, 0, 0, 1);
    drawLeaf(20, 9, 4.7, -0.8, -0.55);
  } glPopMatrix();
  glPushMatrix(); {
    glRotatef(250, 0, 0, 1);
    drawLeaf(20, 9, 3.5, -0.75, -0.5);
  } glPopMatrix();

  glColor3ub(0x4a, 0x94, 0x46);
  glPushMatrix(); {
    glRotatef(-40, 0, 0, 1);
    drawLeaf(20, 8, 4, 0.55, 0.5);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(-0.1, -0.3, -0.1);
    glRotatef(10, 0, 0, 1);
    drawLeaf(20, 9, 4.4, 0.7, 0.5);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(0.2, 0, -0.1);
    glRotatef(55, 0, 0, 1);
    drawLeaf(20, 8, 3.4, 0.4, 0.4);
  } glPopMatrix();
  glPushMatrix(); {
    glTranslatef(0.1, -0.2, 0.1);
    glRotatef(145, 0, 0, 1);
    drawLeaf(20, 9, 4, -0.6, -0.45);
  } glPopMatrix();
  glPushMatrix(); {
    glRotatef(175, 0, 0, 1);
    drawLeaf(20, 12, 5.4, -0.5, -0.55);
  } glPopMatrix();
  glPushMatrix(); {
    glRotatef(210, 0, 0, 1);
    drawLeaf(20, 11, 5.2, -0.75, -0.5);
  } glPopMatrix();
  glPushMatrix(); {
    glRotatef(230, 0, 0, 1);
    drawLeaf(20, 9, 3.9, -0.75, -0.5);
  } glPopMatrix();
  glPushMatrix(); {
    glRotatef(255, 0, 0, 1);
    drawLeaf(20, 11, 3.4, -0.4, -0.35);
  } glPopMatrix();

  glPopMatrix();
}

void drawStalk(float sway) {
  // sway should be between -0.1 to 0.1.
  glPushMatrix();
  glTranslatef(0, 2.25, 0.1);

  float numSections = 10;
  float sectionHeight = 1.2;
  float stalkWidth = 1.4;
  float stalkBendFactor = 0.2;

  float baseX = 0;
  float baseY = 0;
  float direction = sway > 0 ? 1 : -1;

  glPushMatrix(); {
    glBegin(GL_QUAD_STRIP); {
      glColor3ub(0xbf, 0x8c, 0x65);

      // Base.
      glVertex2f((stalkWidth + 1.2) * -direction / 2, 0);
      glVertex2f((stalkWidth + 1.2) * direction / 2, 0);

      // Drawing the stalk upwards.
      for (int section = 1; section <= numSections; section++) {
        glVertex2f(baseX + stalkWidth * -direction / 2,
                   baseY + section * sectionHeight);
        baseY -= abs(sway) * stalkBendFactor * section * 2;
        glVertex2f(baseX + stalkWidth * direction / 2,
                   baseY + section * sectionHeight);
        baseX += sway * section;
        baseY += abs(sway) * stalkBendFactor * section;
      }

      // Neck.
      glVertex2f(baseX + (stalkWidth + 1.2) * -direction / 2,
                 baseY + (numSections + 1) * sectionHeight);
      glVertex2f(baseX + (stalkWidth + 1.2) * direction / 2,
                 baseY + (numSections + 1) * sectionHeight);
    } glEnd();

  } glPopMatrix();

  glPushMatrix(); {
    float angle = -200;
    glTranslatef(baseX, baseY + (numSections + 1.5) * sectionHeight, 0);
    glRotatef(sway * angle, 0, 0, 1);
    drawLeaves(0, 0);
    drawExeggutorHeads(0, 0);
  } glPopMatrix();

  glPopMatrix();
}

float getSwayFromCursor() {
  double maxSway = 0.05;

  float mimikyuX = (20 / 2 + tx * k) / 20;
  float mimikyuY = (20 / 2 + ty * k) / 20;
  float mousePercentX = mouseX / glutGet(GLUT_WINDOW_WIDTH);
  float mousePercentY = 1 - mouseY / glutGet(GLUT_WINDOW_HEIGHT);
  float absAngle = atan2(mousePercentY - mimikyuY, mousePercentX - mimikyuX);
  float relativeAngle = absAngle - alpha * M_DEG_TO_RAD;

  float diffX = (mousePercentX - mimikyuX) * cos(alpha * M_DEG_TO_RAD);
  float diffY = (mimikyuY - mousePercentY) * sin(alpha * M_DEG_TO_RAD);
  float absDistance = sqrt(diffX * diffX + diffY * diffY);
  float relativeDistance = absDistance * cos(relativeAngle);

  return max(-maxSway, min(maxSway, relativeDistance * 2 * maxSway));
}

void drawBulb(float sway) {
  glPushMatrix();
  glTranslatef(sway * 12, 2.8, 0);

  glPushMatrix(); {
    glColor3ub(0xc4, 0xe3, 0x9a);
    renderYellowHead(0.7, 1.2);
  } glPopMatrix();

  glColor3ub(0x4a, 0x94, 0x46);
  glTranslatef(0, 0.8, 0.01);
  glPushMatrix(); {
    glRotatef(-40, 0, 0, 1);
    drawLeaf(20, 6, 1.4, 0.3, 0.3);
  } glPopMatrix();

  glPushMatrix(); {
    glRotatef(205, 0, 0, 1);
    drawLeaf(20, 14, 1.5, -0.3, -0.3);
  } glPopMatrix();

  glPopMatrix();
}

void drawBackground() {
  glPushMatrix();

  glTranslatef(0, -3, -2);

  glPushMatrix(); {  // Water.
    glColor3ub(0x54, 0xaf, 0xda);
    glBegin(GL_POLYGON); {
      glVertex2f(-500, -500);
      glVertex2f(-500, 0);
      glVertex2f(500, 0);
      glVertex2f(500, -500);
    } glEnd();
  } glPopMatrix();

  glPushMatrix(); {  // Island.
    glColor3ub(0xfa, 0xff, 0xa5);
    glRotatef(180, 0, 0, 1);
    glTranslatef(-20, 0, 0.1);
    glBegin(GL_TRIANGLE_STRIP); {
      renderCurveFill(100, 60, 60, -2, 14);
    } glEnd();
  } glPopMatrix();

  glPushMatrix(); {  // Sun.
    glTranslatef(20, 40, 0.2);

    glBegin(GL_TRIANGLE_FAN); {
      glColor3ub(0xff, 0xff, 0xff);
      glVertex2f(0, 0);

      for (float deg = 0; deg <= 360; deg++) {
        float rad = deg * M_DEG_TO_RAD;
        float x = 3 * cos(rad);
        float y = 3 * sin(rad);
        glVertex2f(x, y);
      }

      for (float deg = 0; deg <= 360; deg++) {
        glColor3ub(0xdd, 0xee, 0xff);
        float rad = deg * M_DEG_TO_RAD;
        float x = 9 * cos(rad);
        float y = 9 * sin(rad);
        glVertex2f(x, y);
      }
    } glEnd();
  } glPopMatrix();

  glPopMatrix();
}


void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glPushMatrix();

  // Controls transformation.
  glScalef(k, k, k);
  glTranslatef(tx, ty, 0);
  glRotatef(alpha, 0, 0, 1);

  // Draw your stuff here.
  drawBackground();
  drawMimikyuBase();
  drawMimikyuEyes(getSwayFromCursor());
  drawExeggutorCostumeBase();
  drawStalk(-getSwayFromCursor());
  drawBulb(-getSwayFromCursor());

  glPopMatrix();
  glFlush();
}

void reshape(int w, int h) {
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glOrtho(-10, 10, -10, 10, -10, 10);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void init(void) {
  glClearColor(0.86, 0.93, 1.0, 1.0);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  resetSceneValues();
}

void passiveMotion(int x, int y) {
  mouseX = x;
  mouseY = y;
  glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
  // Keys to control scaling - k.
  // Keys to control rotation - alpha.
  // Keys to control translation - tx, ty.
  switch (key) {
    case 'a':
      alpha += 10;
      glutPostRedisplay();
      break;
    case 'd':
      alpha -= 10;
      glutPostRedisplay();
      break;
    case 'q':
      k = min(5.0, k + 0.1);
      glutPostRedisplay();
      break;
    case 'e':
      k = max(0.2, k - 0.1);
      glutPostRedisplay();
      break;
    case 'z':
      tx = max(-50.0, tx - 1.0);
      glutPostRedisplay();
      break;
    case 'c':
      tx = min(50.0, tx + 1.0);
      glutPostRedisplay();
      break;
    case 's':
      ty = max(-50.0, ty - 1.0);
      glutPostRedisplay();
      break;
    case 'w':
      ty = min(50.0, ty + 1.0);
      glutPostRedisplay();
      break;
    case 'r':
      resetSceneValues();
      glutPostRedisplay();
      break;
    case 27:  // Esc key.
      exit(0);
      break;
    default:
      break;
  }
}

int main(int argc, char **argv) {
  cout << "CS3241 Lab 1\n\n";
  cout << "+++++CONTROL BUTTONS+++++++\n\n";
  cout << "Mouse: Move it around" << endl;
  cout << "Scale Up/Down: Q/E\n";
  cout << "Rotate Clockwise/Counter-clockwise: A/D\n";
  cout << "Move Up/Down: W/S\n";
  cout << "Move Left/Right: Z/C\n";
  cout << "ESC: Quit\n";

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(600, 600);
  glutInitWindowPosition(50, 50);
  glutCreateWindow(argv[0]);

  init();

  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutPassiveMotionFunc(passiveMotion);
  glutKeyboardFunc(keyboard);
  glutMainLoop();

  return 0;
}
