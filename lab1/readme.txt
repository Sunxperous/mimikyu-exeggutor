# Who's that Pokémon?
---
by Sun Wang Jun A0110546R

## Primitives and Transformations Used

GL_LINE_STRIP for circular lines.
GL_POINTS for dots.
GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_POLYGON for curvy, polygonal objects.
GL_QUAD_STRIP for the stalk.

glTranslatef, glRotatef, glScalef.
glColor3ub is used for hexademical colouring instead of glColor3f.

There is a colour gradient involved for the sun in the sky.

There is z-axis translation involved to control the layers where the shapes are
drawn. The values are sufficiently small such that zooming in and out on the
z-axis does not affect rendering.


## Methods Modified

- Keyboard controls are modified to have min/max bounds and greater delta
  values.
- An additional 'r' key resets the view to default.
- Other initialisation methods are slightly modified, mostly to include mouse
control.


## TA Should Know

The TA should make use of all the controls - the drawing is beyond what is
first seen. Do rotate, zoom in/out, move around the display.


## Coolest Things

1. The drawing follows the mouse cursor! Even with rotation enabled, it follows
   in the correct direction - the stalk bends, the heads slightly rotate, the
   eyes and tail move. (Do zoom out sufficiently to view this effect.)

2. Zoom into the middle eyes - they are drawn as circular scribbles using a
   fixed random seed. They are not plain circle fills or outlines.

3. Leaves, mouths and non-middle eyes are drawn using a function that renders a
   curve fill - think of a boomerang shape. This can be better written, but it
   is currently well used.


## Drawing

This is a drawing of a Mimikyu (http://www.pokemon-sunmoon.com/en-ca/pokemon/mimikyu/),
the Ghost/Fairy Disguise Pokémon which originally has a Pikachu costume. On the
island, it now has an Alola Exeggutor costume to celebrate the release of
Pokémon Sun and Moon and the popularity of the new Exeggutor form!
(http://www.pokemon-sunmoon.com/en-ca/pokemon/alolan-exeggutor/)


## Sample Animation

See https://gfycat.com/GaseousEveryHedgehog for a recorded version of this work!
